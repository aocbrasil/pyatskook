from genie.conf import Genie
from pprint import pprint

def wr_configure_interface_description(device):
    logf = open("error.log", "a+")
    try:
        print("Conectando ao dispositivo {}".format(device))
        device.connect()

        cdp_n = device.parse("show cdp neig")

        ###
        # for each neighboor that is not a cisco phone (SEP), set interface description as its hostname + connected interface
        for nei in cdp_n['cdp']['index']:
            if not cdp_n['cdp']['index'][nei]['device_id'].startswith('SEP'):
                nei_dev_id = cdp_n['cdp']['index'][nei]['device_id']
                local_int = cdp_n['cdp']['index'][nei]['local_interface']
                command = f"interface {local_int}\ndescr {local_int} {nei_dev_id}\n"
                device.configure(command)

        print("Desconectando do dispositivo {}".format(device))
        device.disconnect()
        ###
    except Exception as e:
        erro = "Um erro ocorreu ao conectar ao dispositivo {}\n{}\n\n".format(device,str(e))
        logf.write(erro)
        print(erro)
    finally:
        pass
    logf.close()


def load_ssh_keys(device):
    logf = open("error.log", "a+")
    try:
        device.connect(ssh_config=True)
        device.disconnect()
    except Exception as e:
        erro = "Um erro ocorreu ao conectar ao dispositivo {}\n{}\n\n".format(device,str(e))
        logf.write(erro)
        print(erro)
    finally:
        pass
    logf.close()

def connect(device):
    logf = open("error.log", "a+")
    try:
        device.connect(ssh_config=True)
        device.disconnect()
    except Exception as e:
        erro = "Um erro ocorreu ao conectar ao dispositivo {}\n{}\n\n".format(device,str(e))
        logf.write(erro)
        print(erro)
    finally:
        pass
    logf.close()


''' MAIN LOOP '''

testbed = Genie.init("testbed.yaml")

for device in testbed:

    connect(device)
    hostname = device.alias
    shrun =  device.execute("sh vlan br") 
    shintvlan = device.execute("sh ip int br | i Vlan")
    shtrunks = device.execute("sh int trunk")
    output = "\n######   " + hostname + "  ######\n\n" + shrun + '\n\n int vlans \n\n' + shintvlan + '\n\n int trunks \n\n' + shtrunks + '\n\n###############\n'
    filename = "outputsnovo\\vlans_" + hostname + ".ios"
    with open(filename, 'a') as f:
        print(output,file=f)
    device.disconnect()